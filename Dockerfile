FROM alpine:latest

MAINTAINER caner@candan.fr

ENV ROOT_PASSWORD root
ENV BIT_PASSWORD bit

RUN apk update && apk upgrade \
    && apk add --update openssh nodejs npm git \
    && npm i -g bit-bin \
    && mkdir -p /opt/bit/my-scope \
    && mkdir -p /opt/bit/.ssh \
    && adduser -D -u 1000 -h /opt/bit bit \
    && sed -i s/#PermitRootLogin.*/PermitRootLogin\ yes/ /etc/ssh/sshd_config \
    && echo "root:${ROOT_PASSWORD}" | chpasswd \
    && echo "bit:${BIT_PASSWORD}" | chpasswd \
    && rm -rf /var/cache/apk/* /tmp/* \
    && chown -R bit:users /opt/bit

USER bit

RUN bit config set analytics_reporting false \
    && bit config set error_reporting false \
    && cd /opt/bit/my-scope \
    && bit init --bare

USER root

COPY entrypoint.sh /usr/local/bin/

EXPOSE 22

ENTRYPOINT ["entrypoint.sh"]
